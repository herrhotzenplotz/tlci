# Tiny Lambda Calculus Interpreter

In C99.

![](docs/scrot.png)

## Requirements

+ Flex
+ Yacc (Byacc and Bison should work as well)
+ C99 compiler
+ make

## Quick start

```bash
$ make
$ ./tlci
```

## Confirmed to work on

- FreeBSD 12.1-RELEASE amd64
- FreeBSD 13-RELEASE amd64
- FreeBSD 13-CURRENT aarch64
- FreeBSD 13-CURRENT riscv
- GNU/Linux Archlinux amd64
- Debian GNU/Linux amd64
- SunOS/Solaris 10 SPARC Fujitsu VII+
- OpenBSD 6.7 amd64
- Minix 3.3.0 i386

## License

The Tiny Lambda Calculus Interpreter is licensed under a 2-clause
BSD-License. Please refer to the file 'LICENSE' attached.

Copyright 2020 by Nico Sonack.
