/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HEAP_H_
#define HEAP_H_

#include <tlci/string.h>

#define SCOPE_MAX_ENTRIES 64
typedef struct scope {
    unsigned long hashes[SCOPE_MAX_ENTRIES];
    struct heap_cell* references[SCOPE_MAX_ENTRIES];
    size_t end;
} scope_t;

typedef enum cell_type {
    NIL,
    IDENT,
    AP,
    ABS,
    CLOSURE,
    LET,
    RELEASE,
    COMMAND,
    TOKENPOS
} cell_type_t;

typedef struct abstr_cell {
    struct heap_cell* var;
    struct heap_cell* body;
} abstr_cell_t;

typedef struct command_cell {
    struct heap_cell* command;
    struct heap_cell* next;
} command_cell_t;

typedef struct closure_cell {
    struct heap_cell* var;
    struct heap_cell* body;
    scope_t* closed_scope;
} closure_cell_t;

typedef struct token_pos_cell {
    unsigned int line;
    unsigned int col;
    string filename;
} token_pos_cell_t;

typedef struct ident_cell {
    string identifier;
    struct heap_cell* token_pos;
} ident_cell_t;

typedef struct let_cell {
    struct heap_cell* name;
    struct heap_cell* value;
} let_cell_t;

typedef struct app_cell {
    struct heap_cell* function;
    struct heap_cell* argument;
} app_cell_t;

typedef union cell_content {
    ident_cell_t as_ident;
    abstr_cell_t as_abstr;
    app_cell_t as_app;
    let_cell_t as_let;
    struct heap_cell* as_release;
    closure_cell_t as_closure;
    command_cell_t as_command;
    token_pos_cell_t as_token_pos;
} cell_content_t;

typedef struct heap_cell {
    cell_type_t type;
    cell_content_t content;
    int is_alive;
} heap_cell_t;

#define HEAP_POOL_CAPACITY (HEAP_CELL_COUNT * sizeof(heap_cell_t))

typedef struct heap {
    size_t end;
    heap_cell_t* mem_pool;
    size_t free_memory;
} heap_t;

heap_t* heap_create(void);
void heap_garbage_sweep(heap_t*);
heap_cell_t* heap_alloc_ident(heap_t*, heap_cell_t*, string);
heap_cell_t* heap_alloc_let(heap_t*, heap_cell_t*, heap_cell_t*);
heap_cell_t* heap_alloc_release(heap_t*, heap_cell_t*);
heap_cell_t* heap_alloc_ap(heap_t*, heap_cell_t*, heap_cell_t*);
heap_cell_t* heap_alloc_token_pos(heap_t*, string, unsigned int, unsigned int);
heap_cell_t* heap_alloc_abs(heap_t*, heap_cell_t*, heap_cell_t*);
heap_cell_t* heap_alloc_cls(heap_t*, heap_cell_t*, heap_cell_t*, scope_t*);
heap_cell_t* heap_alloc_cmd(heap_t*, heap_cell_t*, heap_cell_t*);
heap_cell_t* heap_alloc_nil(heap_t*);
void heap_print_cell(heap_t*, heap_cell_t*);
void heap_destroy(heap_t*);

#endif
