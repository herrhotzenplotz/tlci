#
# GNU Make and BSD Make compatible Makefile
# This is the preferred way to build TLCI
#

CC?=	cc

# Enable this flag to print heap info after each gc pass
# DEBUG= -D_TLCI_DEBUG_HEAP

# Number of heap cells. You should not change this if you don't know
# what you are doing.
HEAP_CELLS?=	512

CFLAGS+=	-std=iso9899:1999 -pedantic -pedantic-errors \
		-Wall -Wextra -Werror \
		-Iinclude/ -D_XOPEN_SOURCE=600 $(DEBUG) \
		-DHEAP_CELL_COUNT=$(HEAP_CELLS)

LIBS=		-ly
FLEX=		flex
YACC=		yacc

PREFIX?=	/usr/local/
INSTALL?=	install

OBJS=			\
		src/heap.o \
		src/tlci.o \
		src/string.o \
		src/evaluator.o

.PHONY: all
all: tlci

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

tlci: lex.yy.o y.tab.o $(OBJS)
	$(CC) $(CFLAGS) lex.yy.o y.tab.o $(OBJS) -o tlci $(LIBS)

lex.yy.o: src/lexer.l y.tab.h
	$(FLEX) src/lexer.l
	$(CC) $(CFLAGS) -c -Iinclude/ lex.yy.c

y.tab.o y.tab.h: src/parser.y
	$(YACC) -d src/parser.y
	$(CC) $(CFLAGS) -c -Iinclude/ y.tab.c

.PHONY: install
install: all
	$(INSTALL) -d $(DESTDIR)/$(PREFIX)/bin
	$(INSTALL) -C ./tlci $(DESTDIR)/$(PREFIX)/bin
	$(INSTALL) -d $(DESTDIR)/$(PREFIX)/man/man8
	$(INSTALL) -C docs/tlci.8.gz $(DESTDIR)/$(PREFIX)/man/man8

.PHONY: clean
clean:
	rm $(OBJS) lex.yy.o y.tab.o lex.yy.c y.tab.c y.tab.h tlci
