%{
#include <stdio.h>
#include <stdlib.h>
#include <tlci/heap.h>
#include <tlci/string.h>
extern heap_t* tlci_heap;
extern int yylex();
extern void yyerror(const char*);
#define mkAP(f, a) \
    heap_alloc_ap(tlci_heap, f, a)
#define mkABS(var, body) \
    heap_alloc_abs(tlci_heap, var, body)
#define mkID(name) \
    heap_alloc_ident(tlci_heap, name)
#define mkRELEASE(name) \
    heap_alloc_release(tlci_heap, name)
#define mkLET(name, value) \
    heap_alloc_let(tlci_heap, name, value)
#define pushCOMMAND(cmd, next) \
    heap_alloc_cmd(tlci_heap, cmd, next)
#define singleCOMMAND(cmd) \
    heap_alloc_cmd(tlci_heap, cmd, heap_alloc_nil(tlci_heap))
 %}

%union {
    heap_cell_t* cell;
}

%type <cell> abstr
%type <cell> appl
%type <cell> id
%type <cell> command
%type <cell> input
%token <cell> TOK_IDENTIFIER
%token TOK_OPAREN TOK_CPAREN TOK_LAMBDA TOK_DOT TOK_LET TOK_RELEASE TOK_EQ NL
%%

input      : command NL input                         { $$ = pushCOMMAND($1, $3); }
           | command                                  { $$ = singleCOMMAND($1); }
           |                                          { $$ = heap_alloc_nil(tlci_heap); }
           ;

command    : TOK_LET TOK_IDENTIFIER TOK_EQ abstr      { $$ = mkLET($2, $4); }
           | TOK_RELEASE TOK_IDENTIFIER               { $$ = mkRELEASE($2); }
           | abstr                                    { $$ = $1;            }
           ;

abstr      : TOK_LAMBDA TOK_IDENTIFIER TOK_DOT abstr  { $$ = mkABS($2, $4); }
           | appl                                     { $$ = $1;            }
           ;

appl       : appl id                                  { $$ = mkAP($1, $2);  }
           | id                                       { $$ = $1;            }
           ;

id         : TOK_IDENTIFIER                           { $$ = $1;            }
           | TOK_OPAREN abstr TOK_CPAREN              { $$ = $2;            }
           ;
