/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <tlci/heap.h>

/******************************************************************************/
static heap_cell_t* pool_alloc(heap_t*);
static void pool_swipe(heap_t*);
/******************************************************************************/
heap_t* heap_create()
{
    heap_t* heap = calloc(1, sizeof(heap_t));
    if (!heap) {
        fprintf(stderr,
            "FATAL : Could not alloc a managed heap. Download some RAM uwu.\n");
        abort();
    }

    heap->end = 0;

    heap->mem_pool = calloc(1, HEAP_POOL_CAPACITY);
    if (!heap->mem_pool) {
        fprintf(stderr, "FATAL : Couldn't alloc heap memory pool\n");
        free(heap);
        abort();
    }

    heap->free_memory = HEAP_POOL_CAPACITY;

    return heap;
}
/******************************************************************************/
void heap_garbage_sweep(heap_t* heap)
{
#ifdef _TLCI_DEBUG_HEAP
    int no_purged_cells = 0;
#endif
    for (size_t rcell = 0; rcell < HEAP_CELL_COUNT; ++rcell) {
        if (heap->mem_pool[rcell].type == NIL)
            continue;

        if (!heap->mem_pool[rcell].is_alive) {
#ifdef _TLCI_DEBUG_HEAP
            no_purged_cells += 1;
#endif
            if (heap->mem_pool[rcell].type == IDENT) {
                free((void*)heap->mem_pool[rcell].content.as_ident.identifier.begin);
                heap->mem_pool[rcell].content.as_ident.identifier.begin = NULL;
            } else if (heap->mem_pool[rcell].type == TOKENPOS) {
                free((void*)heap->mem_pool[rcell].content.as_token_pos.filename.begin);
                heap->mem_pool[rcell].content.as_token_pos.filename.begin = NULL;
            } else if (heap->mem_pool[rcell].type == CLOSURE) {
                if (heap->mem_pool[rcell].content.as_closure.closed_scope) {
                    free(heap->mem_pool[rcell].content.as_closure.closed_scope);
                    heap->mem_pool[rcell].content.as_closure.closed_scope = NULL;
                }
            }

            heap->mem_pool[rcell].type = NIL;
            heap->free_memory += sizeof(heap_cell_t);
        }
    }
#ifdef _TLCI_DEBUG_HEAP
    printf("\n[Sweep recovered %d cells]\n", no_purged_cells);
#endif
}
/******************************************************************************/
void heap_destroy(heap_t* heap)
{
    if (!heap)
        return;
    pool_swipe(heap);
    if (heap->mem_pool)
        free(heap->mem_pool);
    free(heap);
}
/******************************************************************************/
heap_cell_t* heap_alloc_ident(heap_t* heap, heap_cell_t* tokenpos, string identifier)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = IDENT;
    cell->is_alive = 1;
    cell->content.as_ident.identifier = identifier;
    cell->content.as_ident.token_pos = tokenpos;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_let(heap_t* heap, heap_cell_t* name, heap_cell_t* value)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = LET;
    cell->is_alive = 1;
    cell->content.as_let.name = name;
    cell->content.as_let.value = value;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_release(heap_t* heap, heap_cell_t* name)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = RELEASE;
    cell->is_alive = 1;
    cell->content.as_release = name;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_ap(heap_t* heap, heap_cell_t* fn, heap_cell_t* arg)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = AP;
    cell->is_alive = 1;
    cell->content.as_app.function = fn;
    cell->content.as_app.argument = arg;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_token_pos(heap_t* heap,
    string filename,
    unsigned int line,
    unsigned int col)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = TOKENPOS;
    cell->is_alive = 1;
    cell->content.as_token_pos.filename = filename;
    cell->content.as_token_pos.line = line;
    cell->content.as_token_pos.col = col;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_abs(heap_t* heap, heap_cell_t* var, heap_cell_t* body)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = ABS;
    cell->is_alive = 1;
    cell->content.as_abstr.var = var;
    cell->content.as_abstr.body = body;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_cls(heap_t* heap,
    heap_cell_t* var,
    heap_cell_t* body,
    scope_t* closed_scope)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = CLOSURE;
    cell->is_alive = 1;
    cell->content.as_closure.var = var;
    cell->content.as_closure.body = body;
    cell->content.as_closure.closed_scope = closed_scope;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_cmd(heap_t* heap,
    heap_cell_t* cmd,
    heap_cell_t* next)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = COMMAND;
    cell->is_alive = 1;
    cell->content.as_command.command = cmd;
    cell->content.as_command.next = next;
    heap->end++;
    return cell;
}
/******************************************************************************/
heap_cell_t* heap_alloc_nil(heap_t* heap)
{
    heap_cell_t* cell = pool_alloc(heap);
    cell->type = NIL;
    cell->is_alive = 1;
    heap->end++;
    return cell;
}
/******************************************************************************/
void heap_print_cell(heap_t* heap, heap_cell_t* cell)
{
    switch (cell->type) {
    case IDENT: {
        printf("%s", cell->content.as_ident.identifier.begin);
    } break;
    case AP: {
        putchar('(');
        heap_print_cell(heap, cell->content.as_app.function);
        putchar(' ');
        heap_print_cell(heap, cell->content.as_app.argument);
        putchar(')');
    } break;
    case ABS: {
        putchar('\\');
        heap_print_cell(heap, cell->content.as_abstr.var);
        putchar('.');
        heap_print_cell(heap, cell->content.as_abstr.body);
    } break;
    case CLOSURE: {
        putchar('\\');
        heap_print_cell(heap, cell->content.as_closure.var);
        putchar('.');
        heap_print_cell(heap, cell->content.as_closure.body);
    } break;
    default: {
        printf("<non-printable cell>");
    } break;
    }
}
/******************************************************************************/

/******************************************************************************/
/****** INTERNALS *************************************************************/
/******************************************************************************/
static heap_cell_t* pool_alloc(heap_t* heap)
{
    if (heap->free_memory < sizeof(heap_cell_t)) {
        fprintf(stderr, "FATAL : Out of heap memory. Increase the heap memory size of just don't.\n");
        abort();
    }

    for (size_t i = 0; i < HEAP_CELL_COUNT; ++i) {
        if (!heap->mem_pool[i].is_alive) {
            heap->free_memory -= sizeof(heap_cell_t);
            heap->end = i;
            return &heap->mem_pool[i];
        }
    }

    fprintf(stderr, "FATAL : Out of heap memory. Increase the heap memory size of just don't.\n"
                    "This is a bug, not an issue with the heap memory.\n");
    abort();
}
/******************************************************************************/
static void pool_swipe(heap_t* heap)
{
    for (size_t i = 0; i < HEAP_CELL_COUNT; ++i) {
        if (heap->mem_pool[i].type == IDENT) {
            free((void*)heap->mem_pool[i].content.as_ident.identifier.begin);
            heap->mem_pool[i].content.as_ident.identifier.begin = NULL;
        } else if (heap->mem_pool[i].type == TOKENPOS) {
            free((void*)heap->mem_pool[i].content.as_token_pos.filename.begin);
            heap->mem_pool[i].content.as_token_pos.filename.begin = NULL;
        } else if (heap->mem_pool[i].type == CLOSURE) {
            if (heap->mem_pool[i].content.as_closure.closed_scope) {
                free(heap->mem_pool[i].content.as_closure.closed_scope);
                heap->mem_pool[i].content.as_closure.closed_scope = NULL;
            }
        }
    }

    heap->free_memory = HEAP_POOL_CAPACITY;
    heap->end = 0;
}
/******************************************************************************/
