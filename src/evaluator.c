/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <tlci/evaluator.h>
#include <tlci/heap.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define BEGIN_RED "\033[31m"
#define RESET_COLOR "\033[0m"

#define ERRORF(msg, ...)                          \
    {                                             \
        memset(error_msg, 0, 128 * sizeof(char)); \
        sprintf(error_msg, msg, __VA_ARGS__);     \
        longjmp(on_error, -1);                    \
    }
#define ERROR(msg)                                \
    {                                             \
        memset(error_msg, 0, 128 * sizeof(char)); \
        sprintf(error_msg, msg);                  \
        longjmp(on_error, -1);                    \
    }

/*********************************************************************************/
static char error_msg[128];
static jmp_buf on_error;
static unsigned long djb2(string);
static void mark_cell(heap_t*, heap_cell_t*);
/*********************************************************************************/
heap_cell_t* eval_evaluate(heap_t* heap, scope_t* scope, heap_cell_t* cell)
{
    switch (cell->type) {
    case IDENT: {
        heap_cell_t* lookup = eval_lookup_var(scope, cell->content.as_ident.identifier);
        if (!lookup) {
            ident_cell_t* id_cell = &cell->content.as_ident;
            assert(id_cell->token_pos->type == TOKENPOS);
            token_pos_cell_t* token_cell = &id_cell->token_pos->content.as_token_pos;

            ERRORF("%s:%d:%d: " BEGIN_RED "error:" RESET_COLOR " Unbound value '%s'",
                token_cell->filename.begin,
                token_cell->line,
                token_cell->col,
                id_cell->identifier.begin);
        } else {
            return lookup;
        }
    }
    case CLOSURE:
        return cell;
    case ABS: {
        struct scope* closed_scope = (scope_t*)malloc(sizeof(scope_t));
        if (!closed_scope) {
            fprintf(stderr, BEGIN_RED "FATAL :" RESET_COLOR " Unable to malloc. Please download more RAM.\n");
            abort();
        }

        memcpy(closed_scope, scope, sizeof(scope_t));

        heap_cell_t* closure_cell = heap_alloc_cls(heap,
            cell->content.as_abstr.var,
            cell->content.as_abstr.body,
            closed_scope);

        return closure_cell;
    } break;
    case AP: {

        /* This is the main evaluation mechanism.
         *
         * NOTE:
         *
         * We check first, whether the first argument is an
         * abstraction. Otherwise blow up.
         *
         * Next we solve the argument to the application and bind the
         * result to the var. We can then push a stack frame and
         * continue evaluating. This is a somewhat strict evaluation
         * method but fuck it....
         */

        heap_cell_t* function = eval_evaluate(heap, scope, cell->content.as_app.function);
        if (function->type != CLOSURE) {
            ERROR(BEGIN_RED "error: " RESET_COLOR "Unable to apply a value to a non-closure value");
        }

        heap_cell_t* arg = eval_evaluate(heap, scope, cell->content.as_app.argument);
        heap_cell_t* bind_var = function->content.as_abstr.var;

        if (bind_var->type != IDENT) {
            ERROR(BEGIN_RED "EVALUATOR BUG:" RESET_COLOR " Expected a binder in an abstraction but got a"
                            " non-identifier value. Please report this bug.");
        }

        struct scope* closed_scope = function->content.as_closure.closed_scope;

        eval_insert_scope(closed_scope, bind_var->content.as_ident.identifier, arg);
        heap_cell_t* temp = eval_evaluate(heap,
            closed_scope,
            function->content.as_abstr.body);

        closed_scope->end--;

        return temp;
    } break;
    default: {
        ERROR("Evaluator bug. Found let or release in expression. WTF? Did you break me?");
    };
    }
}
/*********************************************************************************/
void eval_insert_scope(scope_t* scope, string var, heap_cell_t* cell)
{
    if (scope->end == SCOPE_MAX_ENTRIES) {
        ERRORF(BEGIN_RED "oom error:" RESET_COLOR " Scope value storage of %u values exceeded.",
            SCOPE_MAX_ENTRIES);
    }

    unsigned long hash = djb2(var);
    scope->hashes[scope->end] = hash;
    scope->references[scope->end] = cell;
    scope->end += 1;
}
/*********************************************************************************/
heap_cell_t* eval_lookup_var(scope_t* scope, string var)
{
    size_t i;

    /* lookup the hash */
    unsigned long hash = djb2(var);

    for (i = 0; i < scope->end; ++i) {
        if (scope->hashes[i] == hash) {
            return scope->references[i];
        }
    }

    return NULL;
}
/*********************************************************************************/
void eval_release_var(scope_t* scope, string var)
{
    size_t i;
    unsigned long hash = djb2(var);

    for (i = 0; i < scope->end; ++i) {
        if (scope->hashes[i] == hash) {
            break;
        }
    }

    if (i == scope->end) {
        ERRORF("Name '%s' not in global scope", var.begin);
    }

    size_t items_to_move = SCOPE_MAX_ENTRIES - scope->end;
    memmove(&scope->hashes[i], &scope->hashes[i + 1], sizeof(scope->hashes[0]) * items_to_move);
    memmove(&scope->references[i], &scope->references[i + 1], sizeof(scope->references[0]) * items_to_move);
    scope->end--;
}
/*********************************************************************************/
void eval_init(jmp_buf* error_handler)
{
    memcpy(&on_error, error_handler, sizeof(jmp_buf));
}
/*********************************************************************************/
string eval_get_error()
{
    return (string) { .begin = error_msg, .len = strlen(error_msg) };
}
/*********************************************************************************/
void eval_mark(heap_t* heap, scope_t* scope)
{
    for (size_t i = 0; i < HEAP_CELL_COUNT; ++i) {
        heap->mem_pool[i].is_alive = 0;
    }

    for (size_t i = 0; i < scope->end; ++i) {
        mark_cell(heap, scope->references[i]);
    }
}
/******************************************************************************/
static void mark_cell(heap_t* heap, heap_cell_t* cell)
{
    if (cell->is_alive)
        return;

    cell->is_alive = 1;

    switch (cell->type) {
    case AP: {
        mark_cell(heap, cell->content.as_app.function);
        mark_cell(heap, cell->content.as_app.argument);
    } break;
    case ABS: {
        mark_cell(heap, cell->content.as_abstr.var);
        mark_cell(heap, cell->content.as_abstr.body);
    } break;
    case CLOSURE: {
        mark_cell(heap, cell->content.as_closure.var);
        mark_cell(heap, cell->content.as_closure.body);
        for (size_t i = 0;
             i < cell->content.as_closure.closed_scope->end;
             ++i) {
            mark_cell(heap,
                cell->content.as_closure.closed_scope->references[i]);
        }
    } break;
    case COMMAND: {
        mark_cell(heap, cell->content.as_command.command);
        mark_cell(heap, cell->content.as_command.next);
    } break;
    case IDENT: {
        mark_cell(heap, cell->content.as_ident.token_pos);
    } break;
    case TOKENPOS:
    case NIL:
    case RELEASE:
    case LET: {
    } break;
    }
}
/*********************************************************************************/
static unsigned long djb2(string s)
{
    /* stolen from stackoverflow */
    unsigned long hash = 5381;
    for (size_t i = 0; i < s.len; ++i) {
        hash = ((hash << 5) + hash) + s.begin[i];
    }

    return hash;
}
/*********************************************************************************/
