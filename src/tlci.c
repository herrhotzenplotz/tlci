/* Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <tlci/evaluator.h>
#include <tlci/heap.h>

#include "../y.tab.h"

heap_t* tlci_heap;
scope_t global_scope = {
    .end = 0
};
char* filename;

extern int yyparse(void);
extern void reset_lexer(void);
extern void switch_to_stdin(void);
extern FILE* yyin;

/*********************************************************************************/
static void on_quit()
{
    heap_destroy(tlci_heap);
}
/*********************************************************************************/
static void on_sigint(int signal)
{
    (void)signal;
    printf("\n[Received SIGINT]\n");
    exit(EXIT_SUCCESS);
}
/*********************************************************************************/
int yywrap(void)
{
    if (yyin == stdin) {
        printf("\n[Leaving tlci]\n");
        exit(EXIT_SUCCESS);
    }

    return 1;
}
/*********************************************************************************/
static int run_command(heap_cell_t* cell, int should_print_result)
{
    jmp_buf error_handler;

    if (setjmp(error_handler) == -1) {
        /* This is an error in the evaluator */
        return EXIT_FAILURE;

    } else {
        /* We init the evaluator here and call it */
        eval_init(&error_handler);

        switch (cell->type) {
        case CLOSURE:
        case AP:
        case IDENT:
        case ABS: {
            heap_cell_t* result = eval_evaluate(tlci_heap, &global_scope, cell);
            if (should_print_result) {
                fputs(" = ", stdout);
                heap_print_cell(tlci_heap, result);
            }

        } break;
        case LET: {
            let_cell_t* lcell = &cell->content.as_let;
            heap_cell_t* name = lcell->name;

            if (name->type != IDENT) {
                fprintf(stderr, "EVALUATOR BUG: Expected a name for a let-binding but got "
                                "a non-string-typed cell.");
                return EXIT_FAILURE;
            }

            eval_insert_scope(&global_scope, name->content.as_ident.identifier,
                eval_evaluate(tlci_heap, &global_scope, lcell->value));

            if (should_print_result) {
                printf("Bound value '%s'.", name->content.as_ident.identifier.begin);
            }

        } break;
        case RELEASE: {
            heap_cell_t* rcell = cell->content.as_release;
            string name = rcell->content.as_ident.identifier;
            eval_release_var(&global_scope, name);

            if (should_print_result) {
                printf("Released value '%s'.", name.begin);
            }

        } break;
        case TOKENPOS:
        case NIL:
        case COMMAND: {
            fprintf(stderr, "INTERPRETER BUG : Got a NIL or COMMAND cell to run.\n");
            return EXIT_FAILURE;
        } break;
        }

        if (should_print_result) {
            puts("");
        }

        return EXIT_SUCCESS;
    }
}
/*********************************************************************************/
static void run_commands(int should_print)
{
    for (heap_cell_t* cmd_cell = &tlci_heap->mem_pool[tlci_heap->end - 1];
         cmd_cell->type != NIL;
         cmd_cell = cmd_cell->content.as_command.next) {
        heap_cell_t* command = cmd_cell->content.as_command.command;
        if (run_command(command, should_print) == EXIT_FAILURE) {
            string error = eval_get_error();
            fprintf(stderr, "%s\n", error.begin);
            goto error;
        }
    }

error:
    eval_mark(tlci_heap, &global_scope);
    heap_garbage_sweep(tlci_heap);
    reset_lexer();
}
/*********************************************************************************/
int main(int argc, char** args)
{
    puts("tlci - tiny lambda calculus interpreter\n"
         "Copyright 2020 by Nico Sonack\n"
         "https://gitlab.com/herrhotzenplotz/tlci");

    tlci_heap = heap_create();

    if (signal(SIGINT, on_sigint) == SIG_ERR) {
        heap_destroy(tlci_heap);
        fprintf(stderr, "ERR : Unable to setup signal traps\n");
        return EXIT_FAILURE;
    }

    if (atexit(on_quit) < 0) {
        heap_destroy(tlci_heap);
        fprintf(stderr, "ERR : atexit\n");
        return EXIT_FAILURE;
    }

    for (int i = 1; i < argc; ++i) {
        fprintf(stdout, "Loading file %s...\n", args[i]);

        FILE* to_load = fopen(args[i], "r");
        if (!to_load) {
            fprintf(stderr, "ERR : Unable to open file for reading\n");
            return EXIT_FAILURE;
        }

        yyin = to_load;
        filename = args[i];
        int result = yyparse();
        if (result == 0) {
            run_commands(0);
        } else if (result == 1) {
            fclose(to_load);
            continue;
        } else if (result == 2) {
        } else {
            fprintf(stderr, "FATAL : Unknown result from yyparse\n");
            abort();
        }

        fclose(to_load);
    }

    for (;;) {
        reset_lexer();
        switch_to_stdin();
        fputs("> ", stdout);
        int result = yyparse();

        if (result == 0) {
            run_commands(1);
        } else if (result == 1) {
            /* yyerror should have been called by now */
        } else if (result == 2) {
            fprintf(stderr, "ERR : Memory exhaustion\n");
        } else {
            fprintf(stderr, "FATAL : Unknown result from yyparse\n");
            abort();
        }
    }

    return EXIT_SUCCESS;
}
/*********************************************************************************/
