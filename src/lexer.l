%option nounput
%option noinput
%{
#include <tlci/string.h>
#include <stdio.h>
#include <stdlib.h>
#include <tlci/heap.h>
#include "../y.tab.h"
 heap_cell_t* get_token_pos(void);
 extern char* filename;
 extern heap_t* tlci_heap;
 int tokenpos = 0;
 int yyline = 1;
 extern void yyerror(char *);
%}

%%
<<EOF>>                       { return EOF;                             }
let                           { tokenpos += yyleng; return TOK_LET;     }
release                       { tokenpos += yyleng; return TOK_RELEASE; }
"#".*\n                       { }
[A-Za-z_][A-Za-z0-9_\-]*      {   yylval.cell =  heap_alloc_ident(tlci_heap, get_token_pos(), from_cstring(string_dup(yytext)));
                                  tokenpos += yyleng;
                                  return TOK_IDENTIFIER;
                              }


\(                            { tokenpos += yyleng; return TOK_OPAREN; }
=                             { tokenpos += yyleng; return TOK_EQ;     }
\)                            { tokenpos += yyleng; return TOK_CPAREN; }
\\                            { tokenpos += yyleng; return TOK_LAMBDA; }
\.                            { tokenpos += yyleng; return TOK_DOT; }
[ \t]                         { tokenpos += yyleng; /* whitespace */ }
\n                            { if (yyin == stdin) {
                                    return EOF;
                                } else {
                                    tokenpos = 0;
                                    yyline += 1;
                                    return NL;
                                }
                              }
.                             { tokenpos += yyleng; fprintf(stderr, "\033[35mWARN :\033[0m Ignoring unrecognized character: %s\n", yytext); }
%%
void reset_lexer(void)
{
    tokenpos = 0;
    yyline = 1;
    yyin = stdin;
    YY_FLUSH_BUFFER;
}
/*********************************************************************************/
heap_cell_t* get_token_pos()
{
    return heap_alloc_token_pos(tlci_heap,
        from_cstring(string_dup(filename)),
        yyline, tokenpos);
}
/*********************************************************************************/
void yyerror(char *s)
{
    printf("%s:%d:%d: \033[31m%s:\033[0m Unexpected '%s'\n", filename, yylineno, tokenpos, s, yytext);
}
/*********************************************************************************/
void switch_to_stdin()
{
    filename = "<interactive>";
    yyin = stdin;
    yy_set_interactive(1);
}
